<?php

namespace galaxy\far\far\away;

class BetterClone implements BetterClone
{
    public string $id;

    /**
     * Звание
     * @var Rank $rank
     */
    public Rank $rank;

    /**
     * Место службы
     * @var Location $dutyStation
     */
    public $dutyStation;

    // Some additional params...

    /**
     * @param array $params
     * @return bool
     */
    public function setMowevmentsParams(array $params): bool
    {
        foreach ($params as $key => $value) {
            if ($this->validateParam($key, $value)) {
                $this->setParam($key, $value);
            }
        }

        return true;
    }

    /**
     * @param array $params
     * @return bool
     */
    public function setThinkingParams(array $params): bool
    {
        foreach ($params as $key => $value) {
            if ($this->validateParam($key, $value)) {
                $this->setParam($key, $value);
            }
        }

        return true;
    }

    /**
     * @param string $key
     * @param mixed $value
     * @return bool
     */
    public function validateParam(string $key, mixed $value): bool
    {
        /**
         * @CODE
         * @CODE
         * @CODE: Validation...
         * @CODE
         * @CODE
         */

        // If not valid
        throw new ValidationException("Invalid parameter '{$key}'");

        // Else
        return true;
    }

    /**
     * @param string $key
     * @param mixed $value
     * @return void
     */
    public function setParam(string $key, mixed $value): void
    {
        $this->$key = $value;
    }

    /**
     * @return void
     */
    public function calculateRank()
    {
        $rank = 'Soldier';
        /**
         * @CODE: Check params... В процессе вычисления $rank может поменяться...
         * @CODE: if (condition) -> $rank = 'Captain';
         * @CODE: if (condition) -> $rank = 'Major';
         * @CODE: if (condition) -> $rank = 'Head Hunter';
         */

        // Пример: после вычислений присваиваем ему какое то звание...
        $this->rank = new Rank($rank);
    }

    /**
     * @return Rank
     */
    public function getRank(): Rank
    {
        return $this->rank;
    }

    /**
     * @return void
     */
    public function defineDutyStation()
    {
        $location = 'Tatuin';
        /**
         * @CODE: Check params... В процессе вычисления $rank может поменяться...
         * @CODE: if (condition) -> $location = 'Death Star';
         * @CODE: if (condition) -> $location = 'Kashyyyk';
         * @CODE: if (condition) -> $location = 'Mandalore';
         * @CODE: if (condition) -> $location = 'Tatauin';
         */

        // Пример: после вычислений присваиваем ему какое то звание...
        $this->dutyStation = new Location($location);
    }

    /**
     * @return Location
     */
    public function getDutyStation(): Location
    {
        return $this->dutyStation;
    }
}

class BetterCloneCloneGenerator
{
    public BobaFett $BobaFett;

    public Param1 $param1;
    public Param2 $param2;
    public Param3 $param3;
    public Param4 $param4;

    public array $conditions = [];

    public function __construct(
        $param1 = null,
        $param2 = null,
        $param3 = null,
        $param4 = null,
        $condition1 = null,
        $condition2 = null,
        $condition3 = null,
        $condition4 = null
    )
    {
        $this->jangoFett = new BobaFett();
        $this->param1 = $param1;
        $this->param2 = $param2;
        $this->param3 = $param3;
        $this->param4 = $param4;

        if ($condition1 instanceof Condition1) {
            $this->conditions[$condition1->getKey()] = $condition1->getValue();
        }

        if ($condition2 instanceof Condition2) {
            $this->conditions[$condition2->getKey()] = $condition2->getValue();
        }

        if ($condition3 instanceof Condition3) {
            $this->conditions[$condition3->getKey()] = $condition3->getValue();
        }

        if ($condition4 instanceof Condition3 || $condition4 instanceof Condition4) {
            $this->conditions[$condition4->getKey()] = $condition4->getValue();
        }
    }

    /**
     * Функция возвращает переданное количество клонов
     *
     * @param int $amount
     * @return array
     */
    public function createSomeClones(int $amount): array
    {
        $clones = [];

        for ($i = 0; $i < $amount; $i++) {
            $tp = $this->jangoFett->getThinkingParams();
            $mp = $this->jangoFett->getMowevmentsParams();

            $newClone = new RegularClone();
            if ($newClone->setThinkingParams($tp)) {
                if ($newClone->setMowevmentsParams($mp)) {
                    /**
                     * @CODE
                     * @CODE
                     * @CODE: A few lines of DNA magick code...
                     * @CODE
                     * @CODE
                     */

                    $newClone->incubationTime = 3542400;

                    // При создании что то может пойти не так,
                    // поэтому проверяем клона на качесмтво перед добавление в результирующий массив
                    if ($newClone->someCondition && $newClone->someOtherCondition) {
                        $clones[] = $newClone;
                    }
                }
            }
        }

        return $clones;
    }

    /**
     * @param RegularClone $clone
     * @return void
     */
    public function addGoodAiming(RegularClone $clone)
    {
        /**
         * @CODE
         * @CODE
         * @CODE: A few lines of DNA magick code...
         * @CODE
         * @CODE
         */

        $clone->aim = 100;
    }
}

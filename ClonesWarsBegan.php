<?php

namespace galaxy\far\far\away;

class RegularClone implements RegularCloneInterface
{
    public $generatorParam1;
    public $generatorParam2;
    public $generatorParam3;
    public $generatorParam4;
    public $generatorConditions = [];

    public string $id;

    /**
     * Звание
     * @var Rank $rank
     */
    public Rank $rank;

    /**
     * Место службы
     * @var Location $dutyStation
     */
    public $dutyStation;

    // Some additional params...

    /**
     * @param array $params
     * @return bool
     */
    public function setMowevmentsParams(array $params): bool
    {
        foreach ($params as $key => $value) {
            if ($this->validateParam($key, $value)) {
                $this->setParam($key, $value);
            }
        }

        return true;
    }

    /**
     * @param array $params
     * @return bool
     */
    public function setThinkingParams(array $params): bool
    {
        foreach ($params as $key => $value) {
            if ($this->validateParam($key, $value)) {
                $this->setParam($key, $value);
            }
        }

        return true;
    }

    /**
     * @param string $key
     * @param mixed $value
     * @return bool
     */
    public function validateParam(string $key, mixed $value): bool
    {
        /**
         * @CODE
         * @CODE
         * @CODE: Validation...
         * @CODE
         * @CODE
         */

        // If not valid
        throw new ValidationException("Invalid parameter '{$key}'");

        // Else
        return true;
    }

    /**
     * @param string $key
     * @param mixed $value
     * @return void
     */
    public function setParam(string $key, mixed $value): void
    {
        $this->$key = $value;
    }

    /**
     * @return void
     */
    public function calculateRank()
    {
        $rank = 'Soldier';
        /**
         * @CODE: Check params... В процессе вычисления $rank может поменяться...
         * @CODE: if (condition) -> $rank = 'Captain';
         * @CODE: if (condition) -> $rank = 'Major';
         * @CODE: if (condition) -> $rank = 'Head Hunter';
         */

        // Пример: после вычислений присваиваем ему какое то звание...
        $this->rank = new Rank($rank);
    }

    /**
     * @return Rank
     */
    public function getRank(): Rank
    {
        return $this->rank;
    }

    /**
     * @return void
     */
    public function defineDutyStation()
    {
        $location = 'Tatuin';
        /**
         * @CODE: Check params... В процессе вычисления $rank может поменяться...
         * @CODE: if (condition) -> $location = 'Death Star';
         * @CODE: if (condition) -> $location = 'Kashyyyk';
         * @CODE: if (condition) -> $location = 'Mandalore';
         * @CODE: if (condition) -> $location = 'Tatauin';
         */

        // Пример: после вычислений находим ему место службы...
        $this->dutyStation = $location;
    }

    /**
     * @return Location
     */
    public function getDutyStation(): Location
    {
        return $this->dutyStation;
    }

    /**
     * Функция возвращает переданное количество клонов
     *
     * @param int $amount
     * @return array
     */
    public static function createSomeClones(
        int $amount,
        $param1 = null,
        $param2 = null,
        $param3 = null,
        $param4 = null,
        $condition1 = null,
        $condition2 = null,
        $condition3 = null,
        $condition4 = null
        ): array
    {
        $jangoFett = new JangoFett();
        $this->generatorParam1 = $param1;
        $this->generatorParam2 = $param2;
        $this->generatorParam3 = $param3;
        $this->generatorParam4 = $param4;

        if ($condition1 instanceof Condition1) {
            $this->generatorConditions[$condition1->getKey()] = $condition1->getValue();
        }

        if ($condition2 instanceof Condition2) {
            $this->generatorConditions[$condition2->getKey()] = $condition2->getValue();
        }

        if ($condition3 instanceof Condition3) {
            $this->generatorConditions[$condition3->getKey()] = $condition3->getValue();
        }

        if ($condition4 instanceof Condition3 || $condition4 instanceof Condition4) {
            $this->generatorConditions[$condition4->getKey()] = $condition4->getValue();
        }

        $clones = [];

        for ($i = 0; $i < $amount; $i++) {
            $tp = $jangoFett->getThinkingParams();
            $mp = $jangoFett->getMowevmentsParams();

            $newClone = new RegularClone();
            if ($newClone->setThinkingParams($tp)) {
                if ($newClone->setMowevmentsParams($mp)) {
                    /**
                     * @CODE
                     * @CODE
                     * @CODE: A few lines of DNA magick code...
                     * @CODE
                     * @CODE
                     */

                    $newClone->incubationTime = 3542400;

                    // При создании что то может пойти не так,
                    // поэтому проверяем клона на качесмтво перед добавление в результирующий массив
                    if ($newClone->someCondition && $newClone->someOtherCondition) {
                        $clones[] = $newClone;
                    }
                }
            }
        }

        return $clones;
    }

    /**
     * @param RegularClone $clone
     * @return void
     */
    public static function addGoodAiming(RegularClone $clone)
    {
        /**
         * @CODE
         * @CODE
         * @CODE: A few lines of DNA magick code...
         * @CODE
         * @CODE
         */

        $clone->aim = 100;
    }
}

/**
 * Пример использования 
 */
foreach (RegularClone::createSomeClones(100, $param1, $param2, $param3, $param4, $condition1) as $clone) {
    $dutyStation = Empire::findDutyStation($clone->getRank(), $clone->getDutyStation);
    $dutyStation->appoint($clone);
}